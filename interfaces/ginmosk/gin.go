package ginmosk

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/hashicorp/go-uuid"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/golang-libs/mosk.git/model"
	"io"
	"log"
	"math/rand"
	"net/http"
	"strings"
)

var Engine *gin.Engine

type RequestHandlerFunc func(ctx *gin.Context, descriptor model.RequestTypeDescriptor) error

// СТРОГО для логирования - упрощения чтения логов, НЕ использовать для чего-то серьезного
func randID() string {
	b := make([]byte, 7)
	_, err := rand.Read(b)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%x-%x", b[0:3], b[4:])
}

func collectHeadersToMap(h http.Header) map[string]string {
	headers := make(map[string]string)
	for key, value := range h {
		if len(value) > 0 {
			headers[key] = value[0]
		}
	}
	return headers
}

func notFound(ctx *gin.Context) {
	log.Println("Uri ", ctx.Request.URL.String(), " not found")
	ctx.Status(http.StatusNotFound)
}

func CreateRouter(docEndpoint string, descriptors ...model.RequestTypeDescriptors) *gin.RouterGroup {
	for _, descriptorsPart := range descriptors {
		for _, descriptor := range descriptorsPart.Descriptors {
			Engine.RouterGroup.Handle(descriptor.HttpMethod, descriptor.URI, ginChain(descriptor.ChainStart))
		}
	}

	if docEndpoint != "" {
		Engine.RouterGroup.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	}

	return &Engine.RouterGroup
}

func InitServerMiddlewares(serverMiddlewares ...gin.HandlerFunc) {
	for _, m := range serverMiddlewares {
		Engine.Use(m)
	}
}

func InitDefaultServer(logger gin.HandlerFunc) {
	Engine = gin.New()
	Engine.NoRoute()
	Engine.Use(logger)
	Engine.Use(gin.Recovery())
	Engine.NoRoute(notFound)
}

func RegisterEndpoints(logger gin.HandlerFunc, docEndpoint string, descriptors ...model.RequestTypeDescriptors) {
	InitDefaultServer(logger)
	CreateRouter(docEndpoint, descriptors...)
}

func ginChain(firstStep model.ProcessingStep) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		//start := time.Now()
		//i := 0

		responseData := model.NewResponseData()
		requestData := model.NewRequestData()

		id := ctx.GetHeader("fcbrequestid")
		if id == "" {
			id, _ = uuid.GenerateUUID()
		}

		responseData.Code = http.StatusOK
		responseData.RequestId = id

		requestData.Method = ctx.Request.Method
		requestData.URI = ctx.Request.RequestURI
		requestData.Headers = collectHeadersToMap(ctx.Request.Header)
		requestData.RequestId = id
		requestData.Ctx = ctx.Request.Context()

		// Если формы нет - нам все равно
		var err error
		if strings.Contains(ctx.Request.Header.Get("Content-Type"), "multipart/form-data") {
			requestData.MultipartForm, err = ctx.MultipartForm()
			if err != nil {
				log.Printf("Error getting multipart form - %v", err)
				ctx.AbortWithStatus(http.StatusInternalServerError)
				return
			}
		}

		xRealIP := ctx.Request.Header.Get("X-Real-IP")
		if xRealIP == "" {
			requestData.Source = ctx.Request.Header.Get("X-Forwarded-For")
		}

		//i++
		//log.Println("Gin request fasthttp", i, time.Since(start))

		//args := ctx.Request.URI().QueryArgs()
		args := ctx.Request.URL.Query()
		for key, value := range args {
			if len(value) > 0 {
				requestData.Params[key] = value[0]
				if requestData.AllParams[key] == nil {
					requestData.AllParams[key] = make([]string, 0)
				}
				requestData.AllParams[key] = append(requestData.AllParams[key], value[0])
			}
		}

		requestData.Body, err = io.ReadAll(ctx.Request.Body)
		if err != nil {
			log.Println("Error reading request body")
			// todo?
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		// Named variables from url
		for _, p := range ctx.Params {
			requestData.Params[p.Key] = p.Value
		}

		//i++
		//log.Println("Gin request fasthttp", i, time.Since(start))

		firstStep(&requestData, &responseData)

		//i++
		//log.Println("Gin request fasthttp", i, time.Since(start))

		for hKey, hValue := range responseData.Headers {
			ctx.Header(hKey, hValue)
		}
		//i++
		//log.Println("Gin request fasthttp", i, time.Since(start))

		ctx.Data(responseData.Code, responseData.Headers["Content-TYpe"], responseData.Body)
		//i++
		//log.Println("Gin request fasthttp", i, time.Since(start))
	}
}
