package router

import (
	"gitlab.com/golang-libs/mosk.git/model"
)

var router = make(map[string]model.RequestTypeDescriptor)

func FindMethodByRoute(method string) (val model.RequestTypeDescriptor, ok bool) {
	val, ok = router[method]
	return
}

func AddRoute(method string, descriptor model.RequestTypeDescriptor, descriptors model.RequestTypeDescriptors) {
	if descriptor.DeadLetteringQueue == "" {
		descriptor.DeadLetteringQueue = descriptors.DeadLetteringQueue
	}
	router[method] = descriptor
}
