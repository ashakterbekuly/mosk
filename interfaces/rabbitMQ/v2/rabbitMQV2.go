package v2

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/golang-libs/mosk.git/common"
	"gitlab.com/golang-libs/mosk.git/interfaces/rabbitMQ"
	"gitlab.com/golang-libs/mosk.git/model"
	"log"
	"net/http"
	"time"
)

type RabbitMQProcessor struct {
	host       string
	queueName  string
	connection *amqp.Connection
	channel    *amqp.Channel
	done       chan error
	router     map[string]model.RequestTypeDescriptor
}

func NewRabbitMQProcessor(host, queueName string, router map[string]model.RequestTypeDescriptor) *RabbitMQProcessor {
	return &RabbitMQProcessor{
		host:      host,
		queueName: queueName,
		done:      make(chan error),
		router:    router,
	}
}

func (p *RabbitMQProcessor) connect() error {
	conn, err := amqp.Dial(p.host)
	if err != nil {
		return err
	}

	ch, err := conn.Channel()
	if err != nil {
		return err
	}

	_, err = ch.QueueDeclare(
		p.queueName,
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return err
	}

	p.connection = conn
	p.channel = ch
	return nil
}

func (p *RabbitMQProcessor) processMessage(d amqp.Delivery) error {
	method := rabbitMQ.MethodStruct{}
	// Найти тэг "метод"
	err := json.Unmarshal(d.Body, &method)
	if err != nil {
		return errors.Wrap(err, "")
	}

	if method.Method == "" {
		return errors.Errorf("Пустой метод")
	}

	responseData := model.NewResponseData()
	responseData.RequestId = d.MessageId
	requestData := model.NewRequestData()
	requestData.RequestId = d.MessageId
	requestData.Source = d.UserId

	// Пробить хендлер по роутеру
	val, ok := p.router[method.Method]
	if !ok {
		return errors.Errorf("Метод не найден")
	}

	requestData.Method = val.HttpMethod
	requestData.URI = val.URI
	requestData.Body, err = method.Data.MarshalJSON()
	requestData.Ctx = context.Background()

	// Положим в ГЕТ параметры
	if val.HttpMethod == http.MethodGet {
		err = json.Unmarshal(requestData.Body, &requestData.Params)
		if err != nil {
			return errors.Errorf("Неверный формат сообщения: GET принимает только строки")
		}
	}

	if err != nil {
		return errors.Errorf("Неверный формат сообщения")
	}

	requestData.Params, err = common.GetMapByReqType(val.HttpMethod, method.Data)
	if err != nil {
		return errors.Errorf("Неверный формат сообщения")
	}

	// Преобразовать данные в зависимости от типа запроса
	requestData.Headers = rabbitMQ.CollectHeadersToMap(d.Headers)
	// Вызвать чейн
	// Получить массив байт для ответа
	val.ChainStart(&requestData, &responseData)

	respHeaders := map[string]interface{}{}
	respHeaders["code"] = responseData.Code
	respHeaders["message"] = responseData.Message
	for key, val := range responseData.Headers {
		respHeaders[key] = val
	}

	if responseData.Err != nil && val.DeadLetteringQueue != "" {
		return p.channel.PublishWithContext(context.Background(),
			"",                     // exchange
			val.DeadLetteringQueue, // routing key
			false,                  // mandatory
			false,                  // immediate
			amqp.Publishing{
				ContentType:   "application/json",
				CorrelationId: d.CorrelationId,
				Headers:       d.Headers,
				Body:          d.Body,
			})
	}

	if d.ReplyTo != "" && d.CorrelationId != "" {
		// Отправить обратно, по corellationID
		return p.channel.PublishWithContext(context.Background(),
			"",        // exchange
			d.ReplyTo, // routing key
			false,     // mandatory
			false,     // immediate
			amqp.Publishing{
				ContentType:   "application/json",
				CorrelationId: d.CorrelationId,
				Headers:       respHeaders,
				Body:          responseData.Body,
			})
	}

	return nil
}

func (p *RabbitMQProcessor) startConsuming() {
	for {
		err := p.connect()
		if err != nil {
			log.Printf("Failed to connect to RabbitMQ: %v", err)
			log.Println("Retrying in 5 seconds...")
			time.Sleep(5 * time.Second)
			continue
		}

		msgs, err := p.channel.Consume(
			p.queueName,
			"",    // consumer tag
			false, // auto-ack
			false, // exclusive
			false, // no-local
			false, // no-wait
			nil,   // arguments
		)
		if err != nil {
			log.Fatalf("Failed to register consumer: %v", err)
		}

		go func() {
			for msg := range msgs {
				err = p.processMessage(msg)
				if err != nil {
					log.Printf("Failed to process message: %v", err)
				}

				err = msg.Ack(false)
				if err != nil {
					log.Printf("Failed to ack message: %v", err)
				}
			}
		}()

		log.Println("RabbitMQ processor started successfully.")

		select {
		case <-p.connection.NotifyClose(make(chan *amqp.Error)):
			log.Println("Connection to RabbitMQ closed. Reconnecting...")
		case <-p.done:
			return
		}
	}
}

func InitRabbitMQListener(rabbit rabbitMQ.RabbitConfig, allEndpointsOnConn ...model.RequestTypeDescriptors) {
	// Инициализируем каналы
	for _, endpointsBunch := range allEndpointsOnConn {
		if endpointsBunch.Queue == "" {
			continue
		}

		thisRouter := map[string]model.RequestTypeDescriptor{}
		for _, endpoint := range endpointsBunch.Descriptors {
			if endpoint.URI == "" {
				continue
			}
			thisRouter[endpoint.URI] = endpoint
		}

		if len(thisRouter) == 0 {
			continue
		}

		consumer := NewRabbitMQProcessor(
			fmt.Sprintf("%s://%s:%s@%s/%s", rabbit.Protocol, rabbit.User, rabbit.Password, rabbit.Url, rabbit.VirtualHost),
			endpointsBunch.Queue, thisRouter)

		go consumer.startConsuming()
	}
}
