package kafka

type Topic struct {
	In  string `json:"in" validate:"nonzero"`
	Out string `json:"out" validate:"nonzero"`
}
type KafkaConfig struct {
	Brokers []string         `json:"brokers" validate:"nonzero"`
	Topics  map[string]Topic `json:"topics" validate:"nonzero"`
	GroupID string           `json:"groupId" validate:"nonzero"`
}
