package kafkago

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	"gitlab.com/golang-libs/mosk.git/common"
	kafkacommons "gitlab.com/golang-libs/mosk.git/interfaces/kafka"
	"gitlab.com/golang-libs/mosk.git/interfaces/router"
	"gitlab.com/golang-libs/mosk.git/model"
	"go.uber.org/zap"
	"log/slog"
	"net/http"
	"strconv"
	"time"
)

var DefaultHeaders = map[string]interface{}{"Content-Type": "application/json"}

func processMessage(message *kafka.Message, replyProducer *kafka.Writer, legacyMode bool, defaultMethod model.RequestTypeDescriptor) (err error) {

	id := string(message.Key) + "-" + strconv.FormatInt(message.Time.UnixNano(), 16) + "-" + strconv.FormatInt(message.Offset, 10)
	responseData := model.NewResponseData()
	responseData.RequestId = id
	requestData := model.NewRequestData()
	requestData.RequestId = id
	requestData.Source = id

	// initialize defaults
	requestData.Method = defaultMethod.HttpMethod
	requestData.URI = defaultMethod.URI
	requestData.Body = message.Value
	val := defaultMethod

	// нам могут передать исправленный вариант, даже при включенном LegacyMode
	data, err := common.FindRoute(message.Value)
	if err != nil {
		return errors.Wrap(err, "WRONG_JSON_FORMAT")
	}

	if !legacyMode {
		// Если LegacyMode не включен и метода нет - идут лесом
		if data.GetMethod() == "" {
			return errors.Errorf("METHOD_NOT_FOUND")
		}

		var ok bool
		// Пробить хендлер по роутеру
		val, ok = router.FindMethodByRoute(data.GetMethod())
		if !ok {

		}

		requestData.Method = val.HttpMethod
		requestData.URI = val.URI
		requestData.Body = data.Data
	}

	requestData.Ctx = context.Background()
	// Положим в ГЕТ параметры
	if requestData.Method == http.MethodGet {
		err := json.Unmarshal(requestData.Body, &requestData.Params)
		if err != nil {
			return errors.Errorf("Неверный формат сообщения: GET принимает только строки")
		}
	}

	// Преобразовать заголовки
	requestData.Headers = collectHeaders(message.Headers)
	// Вызвать чейн
	// Получить массив байт для ответа
	val.ChainStart(&requestData, &responseData)

	if replyProducer != nil {
		if replyProducer.Topic == "" {
			slog.Error("No reply topic defined!")
			return err
		}

		msg := kafka.Message{
			Value:   responseData.GetBody(),
			Headers: formKafkaHeaders(responseData.Headers),
			Time:    time.Now(),
		}

		err = replyProducer.WriteMessages(context.Background(), msg)
		if err != nil {
			slog.Error(fmt.Sprintf("Error replying to message %s, error is %v", id, err),
				"request", id,
			)
		}
	}

	return nil
}

func formKafkaHeaders(headers map[string]string) []kafka.Header {
	res := make([]kafka.Header, 0)
	for k, v := range headers {
		res = append(res, kafka.Header{
			Key:   k,
			Value: []byte(v),
		})
	}

	return res
}

func collectHeaders(headers []kafka.Header) map[string]string {
	res := map[string]string{}
	for _, header := range headers {
		res[header.Key] = string(header.Value)
	}
	return res
}

type KafkaLogger struct{}

func (k KafkaLogger) Printf(s string, i ...interface{}) {
	slog.Info(s, i...)
}

func InitKafkaListener(config kafkacommons.KafkaConfig, allEndpointsOnConn ...model.RequestTypeDescriptors) error {
	ctx := context.Background()

	// Инициализируем каналы
	for _, endpointsBunch := range allEndpointsOnConn {

		if endpointsBunch.LegacyMode.IsON {
			if len(endpointsBunch.Descriptors) != 1 {
				return errors.New("legacy mode can have only one endpoint")
			}
			endpointsBunch.LegacyMode.DefaultMethod = endpointsBunch.Descriptors[0]
		}

		reader := kafka.NewReader(kafka.ReaderConfig{
			GroupID:                config.GroupID,
			Brokers:                config.Brokers,
			Topic:                  endpointsBunch.Queue,
			MinBytes:               16,
			MaxBytes:               10e6,
			CommitInterval:         time.Second,
			PartitionWatchInterval: time.Second,
		})

		for _, endpoint := range endpointsBunch.Descriptors {
			router.AddRoute(endpoint.URI, endpoint, endpointsBunch)
		}

		slog.Debug("KafkaGoConsumer: reader started...", slog.String("queue", endpointsBunch.Queue))

		// пока инициализирую по продьюсеру на консьюмера
		// возможно, имеет смысл в одном продьюсере на все приложение? Is it "kafka way"?
		var writer *kafka.Writer = nil
		if endpointsBunch.ReplyNeeded && endpointsBunch.ReplyQueue != "" {
			writer = &kafka.Writer{
				Addr:        kafka.TCP(config.Brokers...),
				Topic:       endpointsBunch.ReplyQueue,
				Balancer:    &kafka.LeastBytes{},
				Async:       false,
				ErrorLogger: KafkaLogger{},
			}
		}

		go func(processedEndpoints model.RequestTypeDescriptors) {
			for {
				message, err := reader.ReadMessage(ctx)
				if err != nil {
					slog.Error("KafkaGoConsumer: error reading message from kafka",
						slog.Any("error", err))
					break
				}

				slog.Debug("KafkaGoConsumer: message claimed",
					slog.String("message:", string(message.Value)), slog.Time("time", message.Time),
					slog.String("topic", message.Topic))
				err = processMessage(&message, writer, processedEndpoints.LegacyMode.IsON,
					processedEndpoints.LegacyMode.DefaultMethod)
				if err != nil {
					if !errors.Is(err, common.ErrorNotMyMessage) {
						slog.Error("KafkaGoConsumer: failed to process message",
							slog.Int64("offset", message.Offset),
							slog.Any("err", err))

						// todo deadlettering
					}

					continue
				}

				err = reader.CommitMessages(ctx, message)
				if err != nil {
					slog.Error("KafkaGoConsumer: failed to commit message",
						slog.Int64("offset", message.Offset),
						slog.Any("err", err))
				}
				slog.Debug("KafkaGoConsumer: successfully processed and commited - message", slog.Int64("offset", message.Offset))
			}
			// todo DEFER?
			if err := reader.Close(); err != nil {
				slog.Error("KafkaGoConsumer: failed to close reader: %v", slog.Any("err", err))
			}

			zap.S().Fatalf("KafkaGoConsumer: shutting down, something is wrong when reading from kafka")
		}(endpointsBunch)
	}
	return nil
}
