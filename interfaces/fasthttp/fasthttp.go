package fasthttp

import (
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/hashicorp/go-uuid"
	"github.com/valyala/fasthttp"
	"gitlab.com/golang-libs/mosk.git/model"
	"log"
	"math/rand"
	"net/http"
	"time"
)

var Server *fasthttp.Server

type RequestHandlerFunc func(ctx *fasthttp.RequestCtx, descriptor model.RequestTypeDescriptor) error

// СТРОГО для логирования - упрощения чтения логов, НЕ использовать для чего-то серьезного
func randID() string {
	b := make([]byte, 7)
	_, err := rand.Read(b)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%x-%x", b[0:3], b[4:])
}

func collectHeadersToMap(h *fasthttp.RequestHeader) map[string]string {
	headers := make(map[string]string)
	h.VisitAll(func(key, value []byte) {
		if len(value) > 0 {
			headers[string(key)] = string(value)
		}
	})
	return headers
}

func notFound(ctx *fasthttp.RequestCtx) {
	log.Println("Uri ", ctx.URI(), " not found")
	ctx.Response.Header.SetStatusCode(http.StatusNotFound)
}

func CreateRouter(descriptors ...model.RequestTypeDescriptors) *fasthttprouter.Router {
	r := fasthttprouter.New()
	//r.PanicHandler = recoverHandler
	r.NotFound = notFound

	for _, descriptorsPart := range descriptors {
		for _, descriptor := range descriptorsPart.Descriptors {
			r.Handle(descriptor.HttpMethod, descriptor.URI, fasthttpChain(descriptor.ChainStart))
		}
	}
	return r
}

func InitServer(r *fasthttprouter.Router) {
	Server = &fasthttp.Server{
		Handler:              r.Handler,
		ErrorHandler:         nil,
		LogAllErrors:         true,
		ReadTimeout:          15 * time.Second,
		WriteTimeout:         25 * time.Second,
		IdleTimeout:          35 * time.Second,
		MaxKeepaliveDuration: 360 * time.Second,
		Name:                 "Authority Server",
	}
}

func RegisterEndpoints(descriptors ...model.RequestTypeDescriptors) {
	r := CreateRouter(descriptors...)
	InitServer(r)
}

// На случай если что то капитально отвалится
func formHttpError(ctx *fasthttp.RequestCtx, err error) {
	ctx.Response.Header.SetStatusCode(http.StatusInternalServerError)
	log.Println(err)
	return
}

func fasthttpChain(firstStep model.ProcessingStep) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		//start := time.Now()
		//i := 0

		responseData := model.NewResponseData()
		requestData := model.NewRequestData()

		id := string(ctx.Request.Header.Peek("fcbrequestid"))
		if id == "" {
			id, _ = uuid.GenerateUUID()
		}
		responseData.Code = http.StatusOK
		responseData.RequestId = id

		requestData.Method = string(ctx.Method())
		requestData.URI = string(ctx.Path())
		requestData.Headers = collectHeadersToMap(&ctx.Request.Header)
		requestData.RequestId = id
		requestData.Ctx = ctx

		// Если формы нет - нам все равно
		requestData.MultipartForm, _ = ctx.Request.MultipartForm()
		xRealIP := string(ctx.Request.Header.Peek("X-Real-IP"))
		if xRealIP == "" {
			requestData.Source = string(ctx.Request.Header.Peek("X-Forwarded-For"))
		}

		//i++
		//log.Println("Mosk request fasthttp", i, time.Since(start))

		args := ctx.Request.URI().QueryArgs()
		args.VisitAll(func(key, value []byte) {
			if len(value) > 0 {
				requestData.Params[string(key)] = string(value)
				if requestData.AllParams[string(key)] == nil {
					requestData.AllParams[string(key)] = make([]string, 0)
				}
				requestData.AllParams[string(key)] = append(requestData.AllParams[string(key)], string(value))
			}
		})
		requestData.Body = ctx.Request.Body()

		// Named variables from url
		ctx.VisitUserValues(func(k []byte, v interface{}) {
			str, ok := v.(string)
			if ok {
				requestData.Params[string(k)] = str
			}
		})

		//i++
		//log.Println("Mosk request fasthttp", i, time.Since(start))

		firstStep(&requestData, &responseData)
		//i++
		//log.Println("Mosk request fasthttp", i, time.Since(start))

		for hKey, hValue := range responseData.Headers {
			if hKey == "Content-Type" {
				ctx.Response.Header.SetContentType(hValue)
			} else {
				ctx.Response.Header.Add(hKey, hValue)
			}
		}
		//i++
		//log.Println("Mosk request fasthttp", i, time.Since(start))

		ctx.Response.SetStatusCode(responseData.Code)
		ctx.Response.SetBody(responseData.Body)
		//i++
		//log.Println("Mosk request fasthttp", i, time.Since(start))
	}
}
