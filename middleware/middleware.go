package middleware

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/golang-libs/mosk.git"
	"gitlab.com/golang-libs/mosk.git/common"
	"gitlab.com/golang-libs/mosk.git/model"
	"gitlab.com/golang-libs/mosk.git/validation"
	"go.uber.org/zap"
	"net/http"
)

type Middleware func(step model.ProcessingStep) model.ProcessingStep

func GetChainStart(logicStep model.ProcessingStep, middlewares ...Middleware) model.ProcessingStep {
	processingStep := logicStep
	for _, middleware := range middlewares {
		if middleware == nil {
			continue
		}
		processingStep = middleware(processingStep)
	}
	return processingStep
}

func HttpMethod(method string) Middleware {
	return func(ps model.ProcessingStep) model.ProcessingStep {
		return func(req model.RequestData, resp model.ResponseData) {
			if req.GetMethod() != method {
				resp.SetCode(http.StatusMethodNotAllowed)
				return
			}
			ps(req, resp)
		}
	}
}

func AddResponseHeaders(headers map[string]string) Middleware {
	return func(ps model.ProcessingStep) model.ProcessingStep {
		return func(reqData model.RequestData, responseData model.ResponseData) {
			ps(reqData, responseData)
			responseData.AddHeaders(headers)
		}
	}
}

func formErrorResponseDefault(accept string, e error, customCode, customExtCode int, customText string, validationErrors []validation.ValidationErrorInterface) (int, []byte) {
	code := http.StatusInternalServerError
	text := "Неизвестная ошибка"
	extCode := -1

	// Выставлять ОК при ошибке нельзя!
	if customCode != 0 && !common.IsOKCode(customCode) {
		code = customCode
	}

	if customExtCode != 0 {
		extCode = customExtCode
	}

	if customText != "" {
		text = customText
	}
	// Нам нужен реальный текст для дебага
	if mosk.DebugMode {
		// что бы не проскакивали nil
		c := errors.Cause(e)
		if c != nil {
			text = c.Error()
		}
	}

	var errorsResponse []validation.FieldValidationError
	for _, validationError := range validationErrors {
		errorsResponse = append(errorsResponse, validation.FieldValidationError{
			Destination: validationError.GetDestination(),
			Message:     validationError.GetMessage(),
			ErrorType:   validationError.GetErrorType(),
			//Value:       validationError.GetCurrentValue(),
		})
	}

	jsonErr := model.CustomResponse{ResultDescr: text, Result: extCode, Validations: errorsResponse}
	var responseBytes []byte
	switch accept {
	case "application/xml", "text/xml":
		responseBytes, _ = xml.Marshal(&jsonErr)
	default:
		responseBytes, _ = json.Marshal(&jsonErr)
	}
	return code, responseBytes
}

func ErrHandler() Middleware {
	return ErrorHandler(formErrorResponseDefault)
}

type ErrorResponseFormer func(accept string, e error, customCode, customExtCode int, customText string, validationErrors []validation.ValidationErrorInterface) (int, []byte)

func logValidations(validations []validation.ValidationErrorInterface) {
	str := ""
	for _, v := range validations {
		str += fmt.Sprintf("Validation destination: %s Message: %s ErrType: %s", v.GetDestination(), v.GetMessage(), v.GetErrorType())
	}

	zap.S().Info(str)
}

func ErrorHandler(externalResponseFormer ErrorResponseFormer) Middleware {
	return func(ps model.ProcessingStep) model.ProcessingStep {
		return func(reqData model.RequestData, responseData model.ResponseData) {
			accept := reqData.GetHeaders()["Accept"]
			defer func() {
				if r := recover(); r != nil {
					err := mosk.PanicToError(r)
					maskedCode, b := formErrorResponseDefault(accept, err, responseData.GetCode(), responseData.GetExtCode(),
						responseData.GetMessage(), responseData.GetValidationErrors())
					responseData.SetCode(maskedCode)
					responseData.AddHeaders(map[string]string{"Content-Type": "application/json"})
					responseData.SetBody(b)
				}
			}()

			ps(reqData, responseData)

			maskedCode := responseData.GetCode()
			var b []byte
			err := responseData.GetError()
			if err != nil || !common.IsOKCode(responseData.GetCode()) {
				logValidations(responseData.GetValidationErrors())

				maskedCode, b = externalResponseFormer(accept, err, responseData.GetCode(), responseData.GetExtCode(),
					responseData.GetMessage(), responseData.GetValidationErrors())

				responseData.SetBody(b)

				if err != nil {
					zap.S().Errorw(fmt.Sprintf("%v", err),
						"request", responseData.GetRequestId(),
					)
				} else {
					zap.S().Warnw(fmt.Sprintf("%d %s", responseData.GetCode(), responseData.GetMessage()),
						"request", responseData.GetRequestId(),
					)
				}
			}
			responseData.SetCode(maskedCode)
		}
	}
}
