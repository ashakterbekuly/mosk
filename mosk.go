package mosk

import (
	"encoding/json"
	"flag"
	"github.com/pkg/errors"
	"log"
	"os"
	"time"
)

var AppId = os.Args[0][:1]
var DebugMode = false

const DefaultRMQTimeout = 5 * time.Second

func LoadLocalConfig(config interface{}) {
	var path string
	flag.StringVar(&path, "config", "./config/conf.json", "Config path")
	flag.Parse()

	LoadConfig(path, config)
}

// LoadConfig needed for external things, like a tests, when we want to specify the config path manually.
func LoadConfig(path string, config interface{}) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal("Can not open the config file:", err)
	}

	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatal("Error decoding config:", err)
	}
}

func PanicToError(recover interface{}) (err error) {
	if recover != nil {
		switch x := recover.(type) {
		case string:
			err = errors.Errorf(x, "")
		case error:
			err = errors.Wrap(x, "Ошибка из паники")
		default:
			err = errors.Errorf("unknown panic", "")
		}
	}
	return
}
