package model

import "gitlab.com/golang-libs/mosk.git/validation"

type CustomResponse struct {
	Result      int                               `json:"result" xml:"Result"`
	ResultDescr string                            `json:"result_descr" xml:"ResultDescr"`
	Validations []validation.FieldValidationError `json:"validations,omitempty" xml:"Validations,omitempty"`
}
