package model

type Role struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

type Roles []Role
