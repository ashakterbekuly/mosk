package validation

import (
	"errors"
	"github.com/go-playground/validator/v10"
	"go.uber.org/zap"
	"reflect"
	"strings"
)

type ValidationErrorInterface interface {
	GetDestination() string
	GetMessage() string
	GetErrorType() string
	//GetCurrentValue() interface{}
}

type FieldValidationError struct {
	Destination string `json:"destination"`
	Message     string `json:"message"`
	ErrorType   string `json:"error_type"`
	//Value       interface{} `json:"value"`
}

func (f FieldValidationError) GetDestination() string {
	return f.Destination
}

func (f FieldValidationError) GetMessage() string {
	return f.Message
}

func (f FieldValidationError) GetErrorType() string {
	return f.ErrorType
}

//func (f FieldValidationError) GetCurrentValue() interface{} {
//	return f.Value
//}

func JSONNameFunc(fld reflect.StructField) string {
	name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]

	if name == "-" {
		return ""
	}

	return name
}

func removeStructName(str string) string {
	s := strings.Split(str, ".")
	if len(s) == 0 {
		return ""
	}
	return strings.Join(s[1:], ".")
}

// Пока как пример
func ValidateStruct(v *validator.Validate, input interface{}) (result []ValidationErrorInterface, err error) {
	result = make([]ValidationErrorInterface, 0)
	defer func() {
		if r := recover(); r != nil {
			zap.S().Error("Panic on object validation:", r)
			err = errors.New(r.(string))
		}
	}()
	if v == nil {
		v = validator.New()
		v.RegisterTagNameFunc(JSONNameFunc)
	}

	err = v.Struct(input)
	if err != nil {
		// FROM DEVS:
		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return result, err
		}

		for _, validationErr := range err.(validator.ValidationErrors) {
			result = append(result, FieldValidationError{
				Message:     "",
				ErrorType:   validationErr.Tag(),
				Destination: removeStructName(validationErr.Namespace()),
				//Value:       validationErr.Value(),
			})
			//fmt.Println(err.Namespace())
			//fmt.Println(err.Field())
			//fmt.Println(err.StructNamespace())
			//fmt.Println(err.StructField())
			//fmt.Println(err.Tag())
			//fmt.Println(err.ActualTag())
			//fmt.Println(err.Kind())
			//fmt.Println(err.Type())
			//fmt.Println(validationErr.Value())
			//fmt.Println(err.Param())
			//fmt.Println()
		}

		// from here you can create your own error messages in whatever language you wish
		return result, nil
	}
	return result, nil
}
